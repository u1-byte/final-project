@extends('layout.master')

@section('content')
<div class="col-md mt-3">

  @if(session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
  @endif

  <a href="{{ route('posts.create') }}" class="btn btn-block btn-success btn-lg mb-4">Create New Posts</a>
    <!-- Box Comment -->
    @foreach ($data as $key => $value)
    <div class="card card-widget">
      <div class="card-header">
        <div class="user-block">
          
          {{-- masih gagal bedain post user sama punya orang --}}
          @if(Auth::id() == $value->author->id)
            <img class="img-circle" src=" {{ asset('storage/photo/'.Auth::user()->profile->photo) }} " alt="User Image">
          @else
            <img class="img-circle" src="{{ asset('assets/dist/img/user1-128x128.jpg') }}" alt="User Image">
          @endif
          
          <span class="username"><a href="#">{{ $value->author->name }}</a></span>
          <span class="description">Shared {{ $value->created_at }}</span>
          <span class="description">Last Edited {{ $value->updated_at }}</span>
        </div>
        <!-- /.user-block -->
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
            <i class="far fa-circle"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
          </button>
        </div>
        <!-- /.card-tools -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">

        <!-- Attachment -->
        <div class="attachment-block clearfix">
          <div class="attachment-pushed">

            <div class="attachment-text">
              {{ $value->title }}
            </div>
            <!-- /.attachment-text -->
          </div>
          <!-- /.attachment-pushed -->
        </div>
        <!-- /.attachment-block -->

        @if($value->photo)
            <img class="" src=" {{ asset('storage/posts/'.$value->photo) }} " width="300px" alt="Posts Photo">
        @endif

        <!-- post text -->
        <p>
          {{ $value->content }}
        </p>

        <!-- Social sharing buttons -->
        {{-- <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button> --}}
          <form action="{{ route('postlike.update', ['postlike' => Auth::id()]) }}" method="POST">
            @csrf
            @method("PUT")
            <input type="hidden" name="post_id" value="{{$value->id}}">
            @if(in_array($value->id, $liked_posts))
              <button  class="btn btn-primary btn-sm" value="save" ><i class="far fa-thumbs-up"></i> Like</button>
            @else
              <button  class="btn btn-default btn-sm" value="save" ><i class="far fa-thumbs-up"></i> Like</button>
            @endif
        </form>
        <span class="float-right text-muted">{{ $value->post_likes->count() }} likes - {{ $value->comments->count() }} comments</span>
      </div>

      @foreach ($value->comments as $comment)
      <!-- /.card-body -->
      <div class="card-footer card-comments">
        <div class="card-comment">
          <!-- User image -->
          @if($comment->user->profile->photo)
            <img class="img-circle img-sm" src="{{ asset('storage/photo/'.$comment->user->profile->photo) }}" alt="User Image">
          @else
            <img class="img-circle img-sm" src=" {{ asset('assets/dist/img/user1-128x128.jpg') }} " alt="User Image">
          @endif
          <div class="comment-text">
            <span class="username">
              {{$comment->user->name}}
              <span class="text-muted float-right">{{ $comment->created_at }}</span>
            </span><!-- /.username -->
            {{$comment->content}}
            <form action="{{ route('commentlike.update', ['commentlike' => Auth::id()]) }}" method="POST">
                @csrf
                @method("PUT")
                <input type="hidden" name="comment_id" value="{{$comment->id}}">
                @if(in_array($comment->id, $liked_comments))
                  <button  class="btn btn-primary btn-sm" value="save" ><i class="far fa-thumbs-up"></i> Like</button>
                @else
                  <button  class="btn btn-default btn-sm" value="save" ><i class="far fa-thumbs-up"></i> Like</button>
                @endif
            </form>
            <span class="float-right text-muted">{{ $comment->comment_likes->count() }} likes</span>
          </div>
          
          <!-- /.comment-text -->
        </div>
        {{-- <form action="{{route('comments.destroy', ['comment' => $comment->id])}}" method="POST">
          @csrf
          @method("DELETE")
          <div class="delete-buton">
            <input type="submit" value="delete" class="btn btn-danger btn-sm">
          </div>
        </form> --}}
        <!-- /.card-comment -->
      </div>
      @endforeach

      <!-- /.card-footer -->
      <div class="card-footer">
        <form action="{{route('comments.store')}}" method="POST">
          @csrf
          @if(Auth::user()->profile->photo)
            <img class="img-fluid img-circle img-sm" src="{{ asset('storage/photo/'.Auth::user()->profile->photo) }}" alt="Avatar">
          @else
            <img class="img-fluid img-circle img-sm" src=" {{ asset('assets/dist/img/user4-128x128.jpg') }} " alt="Avatar">
          @endif
          <!-- .img-push is used to add margin to elements next to floating images -->
          <div class="img-push">
            <input type="hidden" name="post_id" value="{{ $value->id }}">
            <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment" name="comment">
          </div>
        </form>
      </div>
      <!-- /.card-footer -->
    </div>    
    @endforeach
    
    <!-- /.card -->
</div>    
@endsection