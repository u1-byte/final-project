<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use Auth;
use App\User;
use Storage;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil follower, following
        $following = DB::table('follows')
                        ->where('user_id', Auth::id())->get()
                        ->count();
        $follower = DB::table('follows')
                        ->where('follow_id', Auth::id())->get()
                        ->count();

        //menghitung jumlah post
        $count_post = DB::table('posts')
                        ->where('user_id', Auth::id())
                        ->get()
                        ->count();

        // dd($follower);

        $user = Auth::user();
        $profile = $user->profile;
        return view('profile.index', compact('profile', 'following', 'follower', 'count_post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user)
    {
        // $profile = Profile::firstOrCreate(
        //     ["user_id" =>  $user->id],
        //     ["fullname" => $user->fullname]
        // );

        // return $profile;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // cek foto yang diupdate
        if($request->hasFile('photo')){
            $filename = $request["photo"]->getClientOriginalName();

            if( Auth::user()->profile->photo ){
                Storage::delete('/public/photo/'.Auth::user()->profile->photo);
            }
            $request["photo"]->storeAs('photo', $filename, 'public');
        }else{
            $filename=Auth::user()->profile->photo;
        }
        
        //update
        $profile = Profile::where('id', $id)->update([
            "fullname" => $request["fullname"],
            "address" => $request["address"],
            "phone" => $request["phone"],
            "photo" => $filename
        ]);

        Alert::success('Berhasil!', 'Profile berhasil diupdate!');

        return redirect('/profile');
        // ->with('success', 'Profile Berhasil Diupdate!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function explore(){
        $profiles = Profile::where('user_id', '<>', Auth::id())->get();

        //mengambil id user yang difollow
        $followed_users = Auth::user()->following;
        $followed_users_ids = [];
        foreach ($followed_users as $followed_user){
            $followed_users_ids[] = $followed_user->id;
        }
        return view('profile.explore', compact('profiles', 'followed_users_ids'));
    }

    public function follow($id){
        $unfolllow = DB::table('follows')->where([
                        ['follow_id', '=', $id],
                        ['user_id', '=', Auth::id()]
                    ]);
        if( $unfolllow->first() ){
            $unfolllow->delete();
        }else{
            $follow = DB::table('follows')->insert([
                "user_id" => Auth::id(),
                "follow_id" => $id
            ]);
        }
        return redirect('/explore');
    }
}
