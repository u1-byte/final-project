<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePost extends Model
{
    protected $table = 'post_likes';
    protected $guarded = [];
    public function user(){
        return $this->hasOne('App\User');
    }
}
